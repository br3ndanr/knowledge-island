//Game.c
//Written by team Keen 3/5/2014
//This file defines all the functions and types required to
//play a game of knowledge island! 

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "Game.h"

#define TURN_LIMIT 1000
#define ARC_LIMIT 72
#define CAMPUS_LIMIT 27
#define GO8_LIMIT 8


#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3

#define NUM_OF_RETRAINING_VERTEXS 18

typedef struct _node *Node;

typedef struct _coord  {
    unsigned char x;
    unsigned char y;
} coord;

typedef struct _node {
    action data;
    Node next;
} node;

typedef struct _player {   
    path arcs[ARC_LIMIT]; //records where arc grants have been built
    path campus[CAMPUS_LIMIT]; //records where campuses have been built
    path go8s[GO8_LIMIT]; //records where go8s have been built
    //records how many students of each type they have
    //students are in the same order on the Openlearning rules page.
    //ThD, BPS, B?, MJ, MTV, M$
    int students[6]; 
    int patents; //records how many patents they own
    int papers; // records how many papers they own
} player;

typedef struct _game { 
    //an array to store the type of each tile
    int tileType[NUM_REGIONS]; 
    //an array to store the number on each tile
    int tileValue[NUM_REGIONS]; 
    //we use a player struct for ease of kpi calculation 
    //and to avoid 2d arrays
    player players[3];
    //This linked list will store all previous actions
    //in the game. It is undefined in size length, meaning
    //the game can run forever (until the computer runs out
    //of memory)
    Node actions; 
    //This int stores the turn (NOT THE NUMBER OF ACTIONS)
    //taking (turn % 3) + 1 will give us the current player
    //whose turn it is.
    int turn;
} game;

//String operations
char *stringCopy(char *destination, const char *source, int destSize);
int stringEquality(char *string1, char *string2);
//Array operations
void removeItem(int index, path *arr, int length);
void addItem(path data, path *arr, int length);
//Linked List functions
Node createList(void);
void destroyList(Node n);
void appendList(Node n, action a);
//Converter function!
coord getCoord(path p);
//required for isLegal
int canBuild (Game g, action a, int G08);
int neighbouringARCs (Game g, action a);

//======================[Debugging Functions]=========================
//These functions will be removed once this file is working. They are
//only for debugging.
void pathChars(path p) {
    for (int i = 0; i < PATH_LIMIT; i++) {
        printf("%d ", p[i]);
    }   
    printf("\n");
}

void printPlayer(player p) {
    printf("=======PRINTING PLAYER======\n");
    for (int i = 0; i < ARC_LIMIT; i++) {
        if (stringEquality(p.arcs[i], "NO_PATH") == FALSE) {
            printf("a:(%d,%d) %s\n",getCoord(p.arcs[i]).x, getCoord(p.arcs[i]).y, p.arcs[i]);
        } else {
            printf("a:      NO_PATH\n");
        }
    }
    for (int i = 0; i < CAMPUS_LIMIT; i++) {
        if (stringEquality(p.campus[i], "NO_PATH") == FALSE) {
            printf("c:(%d,%d) %s\n",getCoord(p.campus[i]).x, getCoord(p.campus[i]).y, p.campus[i]);
        } else {
            printf("c:      NO_PATH\n");
        }
    }
    for (int i = 0; i < GO8_LIMIT; i++) {
        if (stringEquality(p.go8s[i], "NO_PATH") == FALSE) {
            printf("g:(%d,%d) %s\n",getCoord(p.go8s[i]).x, getCoord(p.go8s[i]).y, p.go8s[i]);
        } else {
            printf("g:      NO_PATH\n");
        }
    }
    printf("===========END==============\n");
}

//=====================[Function Definitoions]========================
//Used for testing while the program is not yet implemented!
//Need to delete when we finish this file.
/*
   int main(int argc, char *argv[]) {
   int disciplines[NUM_REGIONS] = {0}; 
   int dice[NUM_REGIONS] = {0};
   Game g = newGame(disciplines, dice);

   action a;

   throwDice(g, 0);

   stringCopy(a.destination, "L",PATH_LIMIT);
   a.actionCode = OBTAIN_ARC;
   makeAction(g,a);

   stringCopy(a.destination, "LR",PATH_LIMIT);
   a.actionCode = OBTAIN_ARC;
   makeAction(g,a);

   throwDice(g, 0);

   stringCopy(a.destination, "RRLRL",PATH_LIMIT);
   a.actionCode = OBTAIN_ARC;
   makeAction(g,a);

   stringCopy(a.destination, "RRLR",PATH_LIMIT);
   a.actionCode = OBTAIN_ARC;
   makeAction(g,a);


   printf("Arc L is type: %d\n", getARC(g, "L"));
   printf("Arc LR is type: %d\n", getARC(g, "LR"));
   printf("Arc RLLLLL is type: %d\n", getARC(g, "RLLLLL"));
   printf("Arc RLLLL is type: %d\n", getARC(g, "RLLLL"));
   printf("Arc RRLRL is type: %d\n", getARC(g, "RRLRL"));
   printf("Arc RRLR  is type: %d\n", getARC(g, "RRLR"));

   throwDice(g, 0);


   printf("Player 1 has %d students of type 0\n", getStudents(g,1,0));
   printf("Player 2 has %d students of type 0\n", getStudents(g,2,0));
   printf("Player 3 has %d students of type 0\n", getStudents(g,3,0));
   disposeGame(g);
   return EXIT_SUCCESS;
   }
   */

//[Done]
//Since we can't use string.h (and hence strcpy), we can't easily copy
//given paths into our array of paths. This means we want to implement
//our own version of strcpy. Note we also want to know the size of the
//destination, because buffer overflows are bad!
char *stringCopy(char *destination, const char *source, int destSize) {
    int i = 0;
    //printf("??%p\n",source);
    //copy while making sure we have room for a null char
    //printf("::%d\n", source[i]);
    while (source[i] != 0 && i < destSize-1) {
        destination[i] = source[i];
        i++;
    }
    //printf("!!\n");
    //gotta null terminate
    destination[i] = 0;
    return destination;
}

//[Done]
//Can't use memcmp form string.h, therefore have function for seeing if
//two strings are equal. Returns true if they are the same and false
//if they are not the same
int stringEquality(char *string1, char *string2) {
    int curIndex = 0;
    int equality = TRUE; // true unless we find it to be false
    int isEndOfString = FALSE;
    //loop through strings to see if any characters are not the same
    //and if they are equality is false. If strings are same then exit
    //loop once end of string has been reached
    while (equality == TRUE && isEndOfString == FALSE) {
        if (string1[curIndex] != string2[curIndex]) {
            equality = FALSE;
        } else if (string1[curIndex] == 0 || 
                string2[curIndex] == 0) {
            isEndOfString = TRUE;
        }

        curIndex++;
    }

    return equality;
}

//[Done]
// make a new game, given the disciplines produced by each
// region, and the value on the dice discs in each region.
// note: each array must be NUM_REGIONS long
// eg if you are using my sample game struct above this function
// would need to set the field currentTurn to -1.  (because the turn
// number is -1 at the start of the game)
Game newGame (int discipline[], int dice[]) {
    //Allocate memory for the struct.
    Game newGame = malloc(sizeof(game));
    //Assert the pointer to the struct exists 
    //(That malloc completed successfully).
    assert (newGame != NULL);

    //Set the board to the given values. Note
    //we use the shorthand notation for dereferencing
    //the struct.
    int i = 0;
    while (i < NUM_REGIONS) {
        newGame->tileType[i] = discipline[i];
        newGame->tileValue[i] = dice[i];
        i++;
    }
    //Set turn counter to -1, which means we are
    //in TERRA_NULLIUS stage.
    newGame->turn = -1;    
    //Create an empty action which we will start our
    //actions list with. Note we cannot use actioncode
    //0 as that is already in use. 255 is easilly 
    //itentifiable when looking at memory (All 1's).
    action emptyAction;
    emptyAction.actionCode = 255;
    //We don't have to set any other values because we
    //only use those after looking at the action code.

    newGame->actions = createList();
    newGame->actions->data = emptyAction;


    //i = 0;
    //while (i < TURN_LIMIT) {
    //    newGame->actions->data = emptyAction;
    //    i++;
    //}

    //Now we need to create each player. This is semi
    //difficult because we need to individually assign 
    //each player their campuses with paths. We also want
    //to make sure all the other fields have no values
    //in them.

    //We will start by creating an empty player which
    //contains no papers, arcs, etc...
    player emptyPlayer;
    emptyPlayer.patents = 0;
    emptyPlayer.papers = 0;
    //make an empty path to fill the GO8s campuses and
    //arcs arrays.
    path emptyPath;
    stringCopy(emptyPath, "NO_PATH", PATH_LIMIT);
    //Make sure arrays are empty. Note we use our own
    //version of strcpy here (since we can't use string.h)
    i = 0;
    while (i < GO8_LIMIT) {
        stringCopy(emptyPlayer.go8s[i], emptyPath, PATH_LIMIT);
        i++;
    }
    i = 0;
    while (i < ARC_LIMIT) {
        stringCopy(emptyPlayer.arcs[i], emptyPath, PATH_LIMIT);
        i++;
    }
    i = 0;
    while (i < CAMPUS_LIMIT) {
        stringCopy(emptyPlayer.campus[i], emptyPath, PATH_LIMIT);
        i++;
    }
    //Now we write this to the game's players array.
    //We also give each player their starting students.
    emptyPlayer.students[STUDENT_THD] = 0; //ThD
    emptyPlayer.students[STUDENT_BPS] = 3;//BPS
    emptyPlayer.students[STUDENT_BQN] = 3; //B?
    emptyPlayer.students[STUDENT_MJ] = 1; //MJ
    emptyPlayer.students[STUDENT_MTV] = 1; //MTV
    emptyPlayer.students[STUDENT_MMONEY] = 1; //M$
    i = 0;
    while (i < 3) {
        newGame->players[i] = emptyPlayer; 
        i++;
    }

    //Now we need to assign each player their campuses.

    //We have an interesting dilema here. The campus
    //for the first player (which is at the starting
    //point of all paths) will be represented by an
    //empty path. I'm worried this could cause a lot
    //of bugs. So, we can instead make the path "LB":
    addItem("LB", newGame->players[0].campus, CAMPUS_LIMIT);
    addItem("RLRLRLRLRLL", newGame->players[0].campus, CAMPUS_LIMIT);

    addItem("RRLRL", newGame->players[1].campus, CAMPUS_LIMIT);
    addItem("LRLRLRRLRL", newGame->players[1].campus, CAMPUS_LIMIT);

    addItem("RRLRLLRLRL", newGame->players[2].campus, CAMPUS_LIMIT);
    addItem("LRLRL", newGame->players[2].campus, CAMPUS_LIMIT);

    //printPlayer(newGame->players[0]);
    return newGame;
}

//[Done]
// after week 7 we will talk about implementing this. for now
// you can have it doing nothing
void disposeGame (Game g){
    //Free all linked list elements
    destroyList(g->actions);
    //Free game struct
    free(g);
}

//[Done]
// make the specified action for the current player and update the 
// game state accordingly.  
// The function may assume that the action requested is legal.
// START_SPINOFF is not a legal action here. Codes:
//#define PASS 0
//#define BUILD_CAMPUS 1
//#define BUILD_GO8 2
//#define OBTAIN_ARC 3
//#define START_SPINOFF 4
//#define OBTAIN_PUBLICATION 5
//#define OBTAIN_IP_PATENT 6
//#define RETRAIN_STUDENTS 7
void makeAction (Game g, action a){
    //We need to know things like this
    //We subtract one because playerID's range from 1-3
    //not 0-2, unlike our indexes.
    int player = getWhoseTurn(g) - 1; 
    //Loop iterator.
    int i = 0;

    //Add action to game's history.
    appendList(g->actions, a);

    if (a.actionCode == PASS) { 
        //Do nothing?

    } else if (a.actionCode == BUILD_CAMPUS) {
        addItem(a.destination, g->players[player].campus, 
                CAMPUS_LIMIT);

        //Subtract student cost!
        g->players[player].students[1] -= 1;
        g->players[player].students[2] -= 1;
        g->players[player].students[3] -= 1;
        g->players[player].students[4] -= 1;

    } else if (a.actionCode == BUILD_GO8) {
        //We need to find the existing campus path and remove it.
        //This is difficult because 2 different paths can represent
        //the same coordinates. Hence we convert our path to 
        //coordinates.
        int actionCompleted = 0; //Used for error catching
        while (i < CAMPUS_LIMIT) {
            //Make sure we don't pass an non-existant path to getCoord.
            if (stringEquality(g->players[player].campus[i], "NO_PATH")
                    == FALSE){
                //Sadly we can't compare structs implicitly
                //We have to do it field by field.
                printf("Get coord in BUILD_GO8\n");
                if (getCoord(a.destination).x == 
                        getCoord(g->players[player].campus[i]).x &&
                        getCoord(a.destination).y ==
                        getCoord(g->players[player].campus[i]).y) {



                    //Add item to go8s array
                    addItem(a.destination, g->players[player].go8s, 
                            GO8_LIMIT);

                    //Remove path from campus array
                    removeItem(i, g->players[player].campus, 
                            CAMPUS_LIMIT);

                    //Set our error catch
                    actionCompleted = 1;
                }
            }
            i++;
        }
        //The move should have been legal.
        assert(actionCompleted != 0);

        //Things cost students!
        g->players[player].students[3] -= 2;
        g->players[player].students[5] -= 3;

    } else if (a.actionCode == OBTAIN_ARC) {
        //Add arc
        addItem(a.destination, g->players[player].arcs, PATH_LIMIT);
        //Subtract cost
        g->players[player].students[1] -= 1;
        g->players[player].students[2] -= 1;

    } else if (a.actionCode == START_SPINOFF) {
        //Illegal action.
        assert(FALSE);

    } else if (a.actionCode == OBTAIN_PUBLICATION) {
        g->players[player].papers++;
        //Subtract cost
        g->players[player].students[3] -= 1;
        g->players[player].students[4] -= 1;
        g->players[player].students[5] -= 1;

    } else if (a.actionCode == OBTAIN_IP_PATENT) {
        g->players[player].patents++;
        //Subtract cost
        g->players[player].students[3] -= 1;
        g->players[player].students[4] -= 1;
        g->players[player].students[5] -= 1;

    } else if (a.actionCode == RETRAIN_STUDENTS) {
        int rate = getExchangeRate(g, player, 
                a.disciplineFrom, a.disciplineTo); 

        g->players[player].students[a.disciplineFrom] -= rate;
        g->players[player].students[a.disciplineTo]++;

    } else {
        //Illegal action given.
        printf("Illegal action: %d\n", a.actionCode);
        assert(FALSE);
    }
}
//[Done]
// advance the game to the next turn, 
// assuming that the dice has just been rolled and produced diceScore
// the game starts in turn -1 (we call this state "Terra Nullis") and 
// moves to turn 0 as soon as the first dice is thrown. 
void throwDice (Game g, int diceScore){
    //We use char because we will never need to represent 
    //a number larger than 10 in this array.
    const char hexVerts[NUM_REGIONS][6][2] = {
        {{0,2},{0,3},{0,4},{1,2},{1,3},{1,4}},
        {{0,4},{0,5},{0,6},{1,4},{1,5},{1,6}},
        {{0,6},{0,7},{0,8},{1,6},{1,7},{1,8}},

        {{1,1},{1,2},{1,3},{2,1},{2,2},{2,3}},
        {{1,3},{1,4},{1,5},{2,3},{2,4},{2,5}},
        {{1,5},{1,6},{1,7},{2,5},{2,6},{2,7}},
        {{1,7},{1,8},{1,9},{2,5},{2,6},{2,7}},

        {{2,0},{2,1},{2,2},{3,0},{3,1},{3,2}},
        {{2,2},{2,3},{2,4},{3,2},{3,3},{3,4}},
        {{2,4},{2,5},{2,6},{3,4},{3,5},{3,6}},
        {{2,6},{2,7},{2,8},{3,6},{3,7},{3,8}},
        {{2,8},{2,9},{2,10},{3,8},{3,9},{3,10}},

        {{3,1},{3,2},{3,3},{4,1},{4,2},{4,3}},
        {{3,3},{3,4},{3,5},{4,3},{4,4},{4,5}},
        {{3,5},{3,6},{3,7},{4,5},{4,6},{4,7}},
        {{3,7},{3,8},{3,9},{4,5},{4,6},{4,7}},

        {{4,2},{4,3},{4,4},{5,2},{5,3},{5,4}},
        {{4,4},{4,5},{4,6},{5,4},{5,5},{5,6}},
        {{4,6},{4,7},{4,8},{5,6},{5,7},{5,8}},
    };
    int type = 0; 
    int player = 0;
    int i;
    int j;
    int k;
    //For every player....
    while (player < 3) {
        //printPlayer(g->players[player]);
        //Loop through their campuses.
        i = 0;
        while (i < CAMPUS_LIMIT) {
            //Make sure we don't attempt to calculate for non-existant
            //paths.
            if (stringEquality(g->players[player].campus[i], "NO_PATH")
                    == FALSE){
                //Now loop through the 2d array defined above
                j = 0;
                while (j < NUM_REGIONS) {
                    //Region must have the value rolled.
                    if (g->tileValue[j] == diceScore) {
                        k = 0;
                        //Still looping through 2d array
                        while (k < 6) {
                            //If coords match, then give player a student.
                            if (getCoord(g->players[player].campus[i]).x == 
                                    hexVerts[j][k][0] &&
                                    getCoord(g->players[player].campus[i]).y == 
                                    hexVerts[j][k][1] ) {

                                type = g->tileType[i];
                                g->players[player].students[type]++;
                            }
                            k++;
                        }
                    }
                    j++;
                }
            }
            i++;
        }
        //Now for group of eights.
        i = 0;
        while (i < GO8_LIMIT) {
            //Make sure we don't attempt to calculate for non-existant
            //paths.
            if (stringEquality(g->players[player].go8s[i], "NO_PATH")
                    == FALSE){
                //Now loop through the 2d array defined above
                j = 0;
                while (j < NUM_REGIONS) {
                    //Region must have the value rolled.
                    if (g->tileValue[j] == diceScore) {
                        k = 0;
                        //Still looping through 2d array
                        while (k < 6) {
                            //If coords match, then give player a student.
                            if (getCoord(g->players[player].go8s[i]).x == 
                                    hexVerts[j][k][0] &&
                                    getCoord(g->players[player].go8s[i]).y == 
                                    hexVerts[j][k][1] ) {

                                type = g->tileType[i];
                                g->players[player].students[type] += 2;
                            }
                            k++;
                        }
                    }
                    j++;
                }
            }
            i++;
        }

        player++;
    }

    //Turn things into THD's
    if (diceScore == 7) {

        i = 0;
        while (i < 3) { 
            g->players[i].students[STUDENT_THD] += 
                g->players[i].students[STUDENT_MTV];
            g->players[i].students[STUDENT_THD] += 
                g->players[i].students[STUDENT_MMONEY];

            g->players[i].students[STUDENT_MTV] = 0;
            g->players[i].students[STUDENT_MMONEY] = 0;

            i++;
        }
    }

    g->turn++;
} 

/*  **** Functions which GET data about the game aka GETTERS **** */
//[Done]
// what type of students are produced by the specified region?
// see discipline codes above
int getDiscipline (Game g, int regionID){
    return g->tileType[regionID];
}

//[Done]
// what dice value produces students in the specified region?
// 2..12
int getDiceValue (Game g, int regionID){
    return g->tileValue[regionID];
}

//[Done]
// which university currently has the prestige award for the most ARCs?
// this is NO_ONE until the first arc is purchased after the game 
// has started.
// This is actually a bit more complicated than it seems. We have to
// be able to store the person who most recently attained the award.
// There are a couple of ways to do this. We could store a variable 
// which holds the ID of the player who currently has the points. Or, 
// we could take advantage of our game history array (actions) and 
// calculate who most recently attained rank of 'most ARC's. This is 
// convenient because we will also be able to reuse the code for 
// calculating the most patents.
int getMostARCs (Game g){
    int players[3] = {0};
    int winnerID = NO_ONE;
    int curID = 1;
    Node currentNode = g->actions; 
    //loop through game history
    while (currentNode != NULL) {
        //if the player passed, the next moves are the next player's.
        if (currentNode->data.actionCode == 0) {
            //a neat way to cycle through numbers 1-3 (since 3%3 is 0)
            curID = (curID % 3) + 1;
        } 
        //else if the current player made an ARC
        else if (currentNode->data.actionCode == 3) {
            //Increment their ARC count
            players[curID]++;
            //Here's where the magic happens. If our current player 
            //has more arcs than both other players, they are the 
            //winner. This works because, since we analyse in the 
            //game in chronological order, the player who first gains
            //the lead retains it if someone else gets the same amount
            //of arcs as them.
            //Our if statement uses our nifty equation from before.
            //Note for the second player we have to nest statements
            //instead of increasing numbers: (say if curID was 2)
            // 2 % 3 = 2
            // 2 + 2 = 4    :(
            if (players[curID] > players[(curID % 3) + 1] && 
                    players[curID] > players[(((curID % 3) + 1) % 3) + 1]) {
                winnerID = curID;
            }
        }
        currentNode = currentNode->next;
    }
    return winnerID;
}

//[Done]
// which university currently has the prestige award for the most pubs?
// this is NO_ONE until the first IP is patented after the game 
// has started.
// Copy paste from Function above, change action code :)
int getMostPublications (Game g){
    int players[3] = {0};
    int winnerID = NO_ONE;
    int curID = 1;
    Node currentNode = g->actions; 
    //loop through game history
    while (currentNode != NULL) {
        //if the player passed, the next moves are the next player's.
        if (currentNode->data.actionCode == 0) {
            //a neat way to cycle through numbers 1-3 (since 3%3 is 0)
            curID = (curID % 3) + 1;
        } 
        //else if the current player made an ARC
        else if (currentNode->data.actionCode == 3) {   
            //Increment their ARC count
            players[curID]++;
            //Here's where the magic happens. If our current player 
            //has more arcs than both other players, they are the 
            //winner. This works because, since we analyse in the 
            //game in chronological order, the player who first gains
            //the lead retains it if someone else gets the same amount
            //of arcs as them.
            //Our if statement uses our nifty equation from before.
            //Note for the second player we have to nest statements
            //instead of increasing numbers: (say if curID was 2)
            // 2 % 3 = 2
            // 2 + 2 = 4    :(
            if (players[curID] > players[(curID % 3) + 1] && 
                    players[curID] > players[(((curID % 3) + 1) % 3) + 1]) {
                winnerID = curID;
            }
        }
        currentNode = currentNode->next;
    }
    return winnerID;
}

//[Done]
// return the current turn number of the game -1,0,1, ..
int getTurnNumber (Game g){
    return g->turn;
}

//[Done]
// return the player id of the player whose turn it is 
// the result of this function is NO_ONE during Terra Nullis
int getWhoseTurn (Game g){
    int turnID;
    if (g->turn == -1) {
        turnID = NO_ONE;
    } else {
        turnID = (g->turn % 3) + 1;
        assert(turnID < 4 && turnID > 0);
    }


    return turnID;
}

// return the contents of the given vertex (ie campus code or 
// VACANT_VERTEX)
int  getCampus(Game g, path pathToVertex){
    int player = 0;
    coord vert = getCoord(pathToVertex);
    coord coordOfPath;
    int result = VACANT_VERTEX;
    int i;

    //printf("getCampus executed, path: %s, coord:(%d,%d)\n",pathToVertex, vert.x, vert.y);
    while (player < 3) {
        i = 0;
        while (i < CAMPUS_LIMIT) {
            //Don't calculate for empty path.
            if (stringEquality(g->players[player].campus[i], "NO_PATH") == FALSE){
                //pathChars(g->players[player].campus[i]); 
                //Check campuses
                coordOfPath = getCoord(g->players[player].campus[i]);
                if (vert.x == coordOfPath.x &&
                        vert.y == coordOfPath.y) {
                    //printf(">>path %s makes ",g->players[player].campus[i]);
                    //printf("coord (%d,%d) has passed test\n",coordOfPath.x, coordOfPath.y);
                    //There should only be one campus or 
                    //go8 per vertex. :/
                    if (result != VACANT_VERTEX) {
                        assert(FALSE);
                    }
                    result = player + 1;
                }
            }
            i++;
        }
        i = 0;
        while (i < GO8_LIMIT) {
            //Don't calculate for empty path.
            if (stringEquality(g->players[player].go8s[i], "NO_PATH") == FALSE) {
                //Check group of 8s
                coordOfPath = getCoord(g->players[player].go8s[i]);
                if (vert.x == coordOfPath.x &&
                        vert.y == coordOfPath.y) {
                    //printf(">>path %s makes ",g->players[player].go8s[i]);
                    //printf("coord (%d,%d) has passed test\n",coordOfPath.x, coordOfPath.y);
                    //There should only be one campus or 
                    //go8 per vertex. :/
                    if (result != VACANT_VERTEX) {
                        assert(FALSE);
                    }
                    result = player + 4;
                }
            }
            i++;
        }
        player++;
    }
    return result;
}

// the contents of the given edge (ie ARC code or vacent ARC)
int getARC(Game g, path pathToEdge){
    //printf("\ngetARC called with path %s\n",pathToEdge);
    //Stores the first vert of the given path
    coord givenVert1 = {0,0};
    coord givenVert2 = getCoord(pathToEdge);
    //Used to hold the modified path we create to get
    //the arc's first vert.
    path pathGivenVert;
    //Will hold the first vert of a player's arc. Used 
    //when we compare.
    coord arcVert1;
    coord arcVert2;

    coord emptyCoord = {0,0};
    path pathArcVert;
    int player;
    int arc;
    int returnVal = NO_ONE;

    //In order to protect ourselves from buffer overflows
    //we need to stop iterating once we reach the end of an array
    //The problem with this is the fact that the end of the array
    //might not be NULL. Hence we need to break if we hit the end
    //of the array without finding NULL && the end is not NULL.
    int completed = 0;

    int i = 0;
    while (i < PATH_LIMIT) {
        //Once we're at the end of the path
        if (pathToEdge[i] == 0 && completed == 0) {
            //Vert1 is at the path minus one step. Doing a little
            //stringCopy magic we can tell it to copy everything
            //except the last character.
            stringCopy(pathGivenVert, pathToEdge, i);
            givenVert1 = getCoord(pathGivenVert);
            completed = 1;
        } else if (i == PATH_LIMIT -1) {
            assert(completed != 0);
        }
        i++;
    }
    //printf("Path to vert1 %s\n", pathGivenVert);
    //printf("Given Vert1:(%d,%d) \n", givenVert1.x, givenVert1.y);
    //printf("Given Vert2:(%d,%d) \n", givenVert2.x, givenVert2.y);

    completed = 0;
    player = 0;
    i = 0;
    while (player < 3) {
        arc = 0;
        while (arc < ARC_LIMIT) {
            i = 0;
            arcVert1 = emptyCoord;
            arcVert2 = emptyCoord;
            if (stringEquality(g->players[player].arcs[arc],"NO_PATH") == FALSE) { 
                //printf("Testing arc %d with path %s\n", arc, g->players[player].arcs[arc]);
                while (i < PATH_LIMIT) {
                    //Once we're at the end of the path
                    if (g->players[player].arcs[arc][i] == 0 
                            && completed == 0) {
                        //Vert1 is at the path minus one step. 
                        //Doing a little stringCopy magic we can tell 
                        //it to copy everything except the last character.
                        stringCopy(pathArcVert, 
                                g->players[player].arcs[arc], i);
                        arcVert1 = getCoord(pathArcVert);
                        arcVert2 = getCoord(g->players[player].arcs[arc]);

                        //printf("ARC Vert1:(%d,%d) \n", arcVert1.x, arcVert1.y);
                        //printf("ARC Vert2:(%d,%d) \n", arcVert2.x, arcVert2.y);


                        //Now compare.
                        //If the first and second verts of the given path
                        //are equal to the first and second verts of the 
                        //current arc's path, then they are the same.
                        if (givenVert1.x == arcVert1.x &&
                                givenVert1.y == arcVert1.y &&
                                givenVert2.x == arcVert2.x &&
                                givenVert2.y == arcVert2.y ) {

                            //printf("Passed on test 1.\n");
                            assert(returnVal == NO_ONE);
                            returnVal = player + 1;
                            //If the first and second verts of the given path
                            //are equal to the SECOND and FIRST verts of the 
                            //current arc's path, then they are the same.
                        } else if (givenVert1.x == arcVert2.x &&
                                givenVert1.y == arcVert2.y &&
                                givenVert2.x == arcVert1.x &&
                                givenVert2.y == arcVert1.y ) {

                            //printf("Passed on test 2.\n");
                            assert(returnVal == NO_ONE);
                            returnVal = player + 1;
                        }
                        i = PATH_LIMIT;
                    }
                    i++;
                }
            }
            arc++;
        }
        player++;
    }   

    return returnVal;
}

// returns TRUE if it is legal for the current
// player to make the specified move, FALSE otherwise.
// legal means everything is legal eg when placing a campus consider 
// such things as: 
//   * is the specified path a valid path?
//   * does it lead to a vacent vertex?
//   * under the rules of the game are they allowed to place a 
//     campus at that vertex?  (eg is it adjacent to one of their ARCs?)
//   * does the player have the 4 specific students required to pay for 
//     that campus?
// It is not legal to make any action during Terra Nullis ie 
// before the game has started.
// It is not legal for a player to make the moves OBTAIN_PUBLICATION 
// or OBTAIN_IP_PATENT (they can make the move START_SPINOFF)
int isLegalAction (Game g, action a){
    //Player -= 1 in order to use value as index.
    int player = getWhoseTurn(g) - 1;
    //Assume TRUE unless false.
    int flag = TRUE;

    //below checks you have the prerequisits to build/obtain/retrain
    //each if checks an action option
    //this series of if statements does NOT check 
    //that the location is on the board
    //to do an action you must have the required students
    //and the required prebuilt structures 
    //(ie you need an ARC for a campus)
    if (a.actionCode == PASS) {
        //Do nothing, pass is always legal.
    } else if (a.actionCode == BUILD_CAMPUS) {
        flag = canBuild(g,a,FALSE);
        //below checks for insufficient of required students
        //since only one of each type is required if 0 is the only fail
        //canBuild has already set to true if can build
        if(g->players[player].students[STUDENT_BPS]==0
                ||g->players[player].students[STUDENT_BQN]==0
                ||g->players[player].students[STUDENT_MJ]==0
                ||g->players[player].students[STUDENT_MTV]==0){
            flag = FALSE;
        }

    } else if (a.actionCode == BUILD_GO8) {
        flag = canBuild(g,a,TRUE);
        //below checks for sufficient students
        //canBuild has already set to true if can build
        if (g->players[player].students[STUDENT_MJ]<2
                ||g->players[player].students[STUDENT_MMONEY]<3) {
            flag = FALSE;
        }

    } else if (a.actionCode == OBTAIN_ARC) {
        //Check that no arcs exist at this location
        if (getARC(g, a.destination) != NO_ONE) {
            flag = FALSE;
        }

        //checking for insufficient students
        if (g->players[player].students[STUDENT_BQN]==0
                ||g->players[player].students[STUDENT_BPS]==0){
            flag = FALSE;
        }
        //Check that the player owns a neighboring arc
        if (neighbouringARCs(g,a) == FALSE) {
            //no neighbouring arcs
            flag = FALSE;
        }

    //Since isLegalAction is used to determine if actions which the
    //*player* makes are legal, start spinoff is legal here (only if
    //they have the resources), and specifically obtaining IP /
    //Publications is not. This is the opposite of makeAction, 
    //where START_SPINOFF is legal and obtaining ip/papers is not.
    } else if (a.actionCode == START_SPINOFF) {
        if (g->players[player].students[STUDENT_MJ]==0
                ||g->players[player].students[STUDENT_MTV]==0
                ||g->players[player].students[STUDENT_MMONEY]==0){
            flag = FALSE;
        }

    } else if (a.actionCode == OBTAIN_PUBLICATION) {
        flag = FALSE;

    } else if (a.actionCode == OBTAIN_IP_PATENT) {
        flag = FALSE;

    } else if (a.actionCode == RETRAIN_STUDENTS) {
        //checking for sufficient students
        int exchange = getExchangeRate (g, player, a.disciplineFrom, a.disciplineTo);
            if (g->players[player].students[a.disciplineFrom] < exchange) {
                flag = FALSE;
            } 

    }
    //checks location is valid
    //TODO: change function so it checks that that path NEVER
    //runs off the map, as opposed to ends on the map.
    if (a.actionCode == BUILD_CAMPUS||a.actionCode == OBTAIN_ARC
            ||a.actionCode == BUILD_GO8) {
        path buffer;
        coord position;
        //check coordinate is on the map
        int i = 0;
        while (a.destination[i] != 0) {
            stringCopy(buffer,a.destination,i+1);
            position = getCoord(buffer);
            if (position.x < 2 || position.y > 3) {
                if (position.x < 1 || position.x > 4) {
                    if (position.y < 2 || position.x > 8) { 
                        flag = FALSE;
                    }
                } else {
                    if (position.y < 1 || position.x > 9) {
                        flag = FALSE;
                    }
                }
            }
        }
        
    }

    return flag;
}

//this checks building resources
//the G08 variable allows this function to work for normal and G08 campuses
int canBuild (Game g, action a, int G08) {
    int counter = 0;
    int flag = FALSE;
    int player = getWhoseTurn(g) - 1;

    if (getARC(g, a.destination) == player) {
        flag = TRUE;
    }

    while (counter<CAMPUS_LIMIT) {
        if (stringEquality(g->players[player].campus[counter], a.destination)) {
            if (G08) {
                flag = TRUE;
            } else {
                flag = FALSE;
            }
        }
        counter++;
    }
    counter=0;
    while (counter<GO8_LIMIT) {
        if (stringEquality(g->players[player].go8s[counter], a.destination)) {
            flag = FALSE;
        }
        counter++;
    }
    return flag;
}

//This function takes in a path and modifies it to refer to the adjacent edges
//It then uses getARC to see who (if anyone) owns arcs on those edges
//If the current player owns any arcs on the edges this will return true
int neighbouringARCs (Game g, action a) {
    int player = getWhoseTurn(g) - 1;
    path pathToMod;
    int flag = FALSE;
    int counter = 0;
    //making a copy of the destination string
    while (counter < PATH_LIMIT) {
        pathToMod[counter] = a.destination[counter];
    }
    //these will store paths for the 4 abjacent edges
    path option1;
    path option2;
    path option3;
    path option4;

    int changed = FALSE;
    counter = 0; //resetting for another parse
    //making first option
    while (changed == FALSE) {
        option1[counter] = pathToMod[counter];
        if (option1[counter] != 'L' && option1[counter] != 'R' && option1[counter] != 'B') {
            option1[counter] = 'L';
            changed = TRUE;
        }
        counter++;
    }

    counter = 0;
    changed = FALSE;
    while (changed == FALSE) {
        option2[counter] = pathToMod[counter];
        if (option2[counter] != 'L' && option2[counter] != 'R' && option2[counter] != 'B') {
            option2[counter] = 'R';
            changed = TRUE;
        }
        counter++;
    }

    counter = 0;
    changed = FALSE;
    while (changed == FALSE) {
        option3[counter] = pathToMod[counter];
        if (option3[counter] != 'L' && option3[counter] != 'R' && option3[counter] != 'B') {
            option3[counter] = 'B';
            option3[counter] = 'L';
            changed = TRUE;
        }
        counter++;
    }

    counter = 0;
    changed = FALSE;
    while (changed == FALSE) {
        option4[counter] = pathToMod[counter];
        if (option4[counter] != 'L' && option4[counter] != 'R' && option4[counter] != 'B') {
            option4[counter] = 'B';
            option4[counter] = 'R';
            changed = TRUE;
        }
        counter++;
    }

    if (getARC(g, option1) == player) {
        flag = TRUE;
    } else if (getARC(g, option2) == player) {
        flag = TRUE;
    } else if (getARC(g, option3) == player) {
        flag = TRUE;
    } else if (getARC(g, option4) == player) {
        flag = TRUE;
    }
    return flag;
}
// --- get data about a specified player ---

// return the number of KPI points the specified player currently has
int getKPIpoints (Game g, int player){
    //game.players[player]
    int points = 0;

    if (player != 0) {
        points = points + 2*getARCs(g,player);
        points = points + 10*(getCampuses(g,player));
        points = points + 20*getGO8s(g,player);
        points = points + 10*getIPs(g,player);
        if (player == getMostARCs(g)){
            points = points + 10;
        }
        if (player == getMostPublications(g)){
            points = points + 10;
        }
    }
    return points;
} 


// return the number of ARC grants the specified player currently has
int getARCs (Game g, int player){
    int noARCs = 0;
    while (stringEquality(g->players[player - 1].arcs[noARCs], "NO_PATH") == FALSE ){
        noARCs++;
    }

    return noARCs;
}

// return the number of GO8 campuses the specified player currently has
int getGO8s (Game g, int player){ 
    int noG08s = 0;
    while (stringEquality(g->players[player - 1].go8s[noG08s], "NO_PATH") == FALSE ){
        noG08s++;
    }

    return noG08s;
}

// return the number of normal Campuses the specified player currently has
int getCampuses (Game g, int player){
    int noCampuses = 0;
    while (stringEquality(g->players[player - 1].campus[noCampuses], "NO_PATH") == FALSE ){
        noCampuses++;
    }

    return noCampuses;
}

//[Done]
// return the number of IP Patents the specified player currently has
int getIPs (Game g, int player){
    return g->players[player - 1].patents;
} 

// return the number of Publications the specified player currently has
int getPublications (Game g, int player){
    return g->players[player - 1].papers;
} 

//[Done]
// return the number of students of the specified discipline type 
// the specified player currently has
int getStudents (Game g, int player, int discipline){
    //Remembering that the index of a player is less 1 of their ID.
    return g->players[player - 1].students[discipline];
} 

//[Done] FINALLY!!!
// student of discipline type disciplineTo.  This will depend
// on what retraining centers, if any, they have a campus at.
int getExchangeRate (Game g, int player,
        int disciplineFrom, int disciplineTo){   
    // SECTION 1:
    // Create coordinate array containing every retraining centre
   
    // store retraining vertexs so I can loop through them
    coord retrainingVertexs[NUM_OF_RETRAINING_VERTEXS];    
   
    int curRetrainVertex = 0;
   
    // assign the coords of each vertex starting from
    // coordinate 3,1 (see README) and going clockwise
    while (curRetrainVertex < NUM_OF_RETRAINING_VERTEXS) {
        if (curRetrainVertex == 0) {
            retrainingVertexs[curRetrainVertex].x = 3;
            retrainingVertexs[curRetrainVertex].y = 1;
        } else if (curRetrainVertex == 1) {
            retrainingVertexs[curRetrainVertex].x = 4;
            retrainingVertexs[curRetrainVertex].y = 1;
        } else if (curRetrainVertex == 2) {
            retrainingVertexs[curRetrainVertex].x = 5;
            retrainingVertexs[curRetrainVertex].y = 2;
        } else if (curRetrainVertex == 3) {
            retrainingVertexs[curRetrainVertex].x = 6;
            retrainingVertexs[curRetrainVertex].y = 3;
        } else if (curRetrainVertex == 4) {
            retrainingVertexs[curRetrainVertex].x = 5;
            retrainingVertexs[curRetrainVertex].y = 5;
        } else if (curRetrainVertex == 5) {
            retrainingVertexs[curRetrainVertex].x = 5;
            retrainingVertexs[curRetrainVertex].y = 6;
        } else if (curRetrainVertex == 6) {
            retrainingVertexs[curRetrainVertex].x = 4;
            retrainingVertexs[curRetrainVertex].y = 8;
        } else if (curRetrainVertex == 7) {
            retrainingVertexs[curRetrainVertex].x = 4;
            retrainingVertexs[curRetrainVertex].y = 9;
        } else if (curRetrainVertex == 8) {
            retrainingVertexs[curRetrainVertex].x = 3;
            retrainingVertexs[curRetrainVertex].y = 10;
        } else if (curRetrainVertex == 9) {
            retrainingVertexs[curRetrainVertex].x = 2;
            retrainingVertexs[curRetrainVertex].y = 10;
        } else if (curRetrainVertex == 10) {
            retrainingVertexs[curRetrainVertex].x = 1;
            retrainingVertexs[curRetrainVertex].y = 9;
        } else if (curRetrainVertex == 11) {
            retrainingVertexs[curRetrainVertex].x = 1;
            retrainingVertexs[curRetrainVertex].y = 8;
        } else if (curRetrainVertex == 12) {
            retrainingVertexs[curRetrainVertex].x = 0;
            retrainingVertexs[curRetrainVertex].y = 6;
        } else if (curRetrainVertex == 13) {
            retrainingVertexs[curRetrainVertex].x = 0;
            retrainingVertexs[curRetrainVertex].y = 5;
        } else if (curRetrainVertex == 14) {
            retrainingVertexs[curRetrainVertex].x = 0;
            retrainingVertexs[curRetrainVertex].y = 3;
        } else if (curRetrainVertex == 15) {
            retrainingVertexs[curRetrainVertex].x = 0;
            retrainingVertexs[curRetrainVertex].y = 2;
        } else if (curRetrainVertex == 16) {
            retrainingVertexs[curRetrainVertex].x = 1;
            retrainingVertexs[curRetrainVertex].y = 1;
        } else if (curRetrainVertex == 17) {
            retrainingVertexs[curRetrainVertex].x = 2;
            retrainingVertexs[curRetrainVertex].y = 1;
        }
    }
   
    // coords to store disciplineTo retraining vertexs
    coord disciplineToVertex1;
    coord disciplineToVertex2;
   
    disciplineToVertex1.x = 0;
    disciplineToVertex1.y = 0;
    disciplineToVertex2.x = 0;
    disciplineToVertex2.y = 0;
   
    // store disciplineTo retraining vertexs   
    if (disciplineTo == STUDENT_MMONEY) {
        disciplineToVertex1.x = 3;
        disciplineToVertex1.y = 1;
        disciplineToVertex2.x = 4;
        disciplineToVertex2.y = 1;
    } else if (disciplineTo == STUDENT_BQN) {
        disciplineToVertex1.x = 5;
        disciplineToVertex1.y = 5;
        disciplineToVertex2.x = 5;
        disciplineToVertex2.y = 6;
    } else if (disciplineTo == STUDENT_MJ) {
        disciplineToVertex1.x = 4;
        disciplineToVertex1.y = 8;
        disciplineToVertex2.x = 4;
        disciplineToVertex2.y = 9;
    } else if (disciplineTo == STUDENT_BPS) {
        disciplineToVertex1.x = 1;
        disciplineToVertex1.y = 9;
        disciplineToVertex2.x = 1;
        disciplineToVertex2.y = 8;
    } else if (disciplineTo == STUDENT_MTV) {
        disciplineToVertex1.x = 1;
        disciplineToVertex1.y = 1;
        disciplineToVertex2.x = 2;
        disciplineToVertex2.y = 1;
    }
    
    // coords to store general retraining vertexs
    coord generalVertex1;
    coord generalVertex2;
    coord generalVertex3;
    coord generalVertex4;
    coord generalVertex5;
    coord generalVertex6;
    coord generalVertex7;
    coord generalVertex8;
    
    generalVertex1.x = 5;
    generalVertex1.y = 2;
    generalVertex2.x = 6;
    generalVertex2.y = 3;
    generalVertex3.x = 3;
    generalVertex3.y = 10;
    generalVertex4.x = 2;
    generalVertex4.y = 10;
    generalVertex5.x = 0;
    generalVertex5.y = 6;
    generalVertex6.x = 0;
    generalVertex6.y = 5;
    generalVertex7.x = 0;
    generalVertex7.y = 3;
    generalVertex8.x = 0;
    generalVertex8.y = 2;
   
   
    // SECTION 2:
    // Create coordinate arrays for player's campuses and g08s
   
    // find current player
    int curPlayer = getWhoseTurn(g);
   
    // find out how many campuses player has
    int campusNo = getCampuses(g, curPlayer);

    // store player's campuses
    coord playerCampuses[campusNo];
   
    int counter = 0;

    // convert the player's campuses to coordinates and store them  
    while (counter < campusNo) {
        playerCampuses[counter] = getCoord(g->players[curPlayer+1].campus[counter]);
        counter++;
    }   

    // find out how many g08s player has
    int g08No = getGO8s(g, curPlayer);

    // store player's g08s
    coord playerGO8s[g08No];
   
    counter = 0;

    // convert the player's g08s to coordinates and store them  
    while (counter < g08No) {
        playerGO8s[counter] = getCoord(g->players[curPlayer+1].go8s[counter]);
        counter++;
    }
   
    // SECTION 3:
    // Find if player does or doesn't have a campus or g08 on a
    // disciplineTo retraining centre or a general retraining centre
    // and assign the exchange rate accordingly
    
    int haveDisciplineToExchange = FALSE;
    int haveGeneralExchange = FALSE;   
   
    counter = 0;
   
    // loop through player campuses
    // if player has campus on a disciplineTo retraining centre
    // then exchangeRate = 2
    // if player has campus on general retraining centre
    // then exchangeRate = 3
    // if player has neither of the above
    // then exchangeRate = 4
    
    int counter1 = 0;
    int counter2 = 0;
    
    int exchangeRate = 9001; // IT'S OVER 9000!
   
    while (counter1 < campusNo) {
        while (counter2 < NUM_OF_RETRAINING_VERTEXS) {
            if (playerCampuses[counter1].x == retrainingVertexs[counter2].x
                && playerCampuses[counter1].y == retrainingVertexs[counter2].y) {
                
                  // first if: check if player campus is disiciplineTo vertex
                  // if so then then haveDisciplineToExchange is true
                  // second if: check if player campus is general retraining
                  // vertex, if so then haveGeneralExchange is true
                if ((playerCampuses[counter1].x == disciplineToVertex1.x
                    && playerCampuses[counter1].y == disciplineToVertex1.y)
                    || (playerCampuses[counter1].x == disciplineToVertex2.x
                    && playerCampuses[counter1].y == disciplineToVertex2.y)) {
                      
                      haveDisciplineToExchange = TRUE;                      
                } else if ((playerCampuses[counter1].x == generalVertex1.x
                    && playerCampuses[counter1].y == generalVertex1.y)
                    || (playerCampuses[counter1].x == generalVertex2.x
                    && playerCampuses[counter1].y == generalVertex2.y)
                    || (playerCampuses[counter1].x == generalVertex3.x
                    && playerCampuses[counter1].y == generalVertex3.y)
                    || (playerCampuses[counter1].x == generalVertex4.x
                    && playerCampuses[counter1].y == generalVertex4.y)
                    || (playerCampuses[counter1].x == generalVertex5.x
                    && playerCampuses[counter1].y == generalVertex5.y)
                    || (playerCampuses[counter1].x == generalVertex6.x
                    && playerCampuses[counter1].y == generalVertex6.y)
                    || (playerCampuses[counter1].x == generalVertex7.x
                    && playerCampuses[counter1].y == generalVertex7.y)
                    || (playerCampuses[counter1].x == generalVertex8.x
                    && playerCampuses[counter1].y == generalVertex8.y)) {
                    
                    haveGeneralExchange = TRUE;
                } 
            }     
          
            counter2++;
        }
  
        counter1++;
    }
    
    
    // loop through player go8s only if player doesn't have min exchange rate
    // (would be pointless to loop through all of it all over again)
    // if player has go8 on a disciplineTo retraining centre
    // then exchangeRate = 2
    // if player has go8 on general retraining centre
    // then exchangeRate = 3
    // if player has neither of the above
    // then exchangeRate = 4
    
    counter1 = 0;
    counter2 = 0;
    
    if (haveDisciplineToExchange == FALSE) {
        while (counter1 < g08No) {
            while (counter2 < NUM_OF_RETRAINING_VERTEXS) {
                if (playerGO8s[counter1].x == retrainingVertexs[counter2].x
                    && playerGO8s[counter1].y == retrainingVertexs[counter2].y) {
                    
                      // first if: check if player go8 is disiciplineTo vertex
                      // if so then then haveDisciplineToExchange is true
                      // second if: check if player go8 is general retraining
                      // vertex, if so then haveGeneralExchange is true
                    if ((playerGO8s[counter1].x == disciplineToVertex1.x
                        && playerGO8s[counter1].y == disciplineToVertex1.y)
                        || (playerGO8s[counter1].x == disciplineToVertex2.x
                        && playerGO8s[counter1].y == disciplineToVertex2.y)) {
                          
                          haveDisciplineToExchange = TRUE;                      
                    } else if ((playerGO8s[counter1].x == generalVertex1.x
                        && playerGO8s[counter1].y == generalVertex1.y)
                        || (playerGO8s[counter1].x == generalVertex2.x
                        && playerGO8s[counter1].y == generalVertex2.y)
                        || (playerGO8s[counter1].x == generalVertex3.x
                        && playerGO8s[counter1].y == generalVertex3.y)
                        || (playerGO8s[counter1].x == generalVertex4.x
                        && playerGO8s[counter1].y == generalVertex4.y)
                        || (playerGO8s[counter1].x == generalVertex5.x
                        && playerGO8s[counter1].y == generalVertex5.y)
                        || (playerGO8s[counter1].x == generalVertex6.x
                        && playerGO8s[counter1].y == generalVertex6.y)
                        || (playerGO8s[counter1].x == generalVertex7.x
                        && playerGO8s[counter1].y == generalVertex7.y)
                        || (playerGO8s[counter1].x == generalVertex8.x
                        && playerGO8s[counter1].y == generalVertex8.y)) {
                        
                        haveGeneralExchange = TRUE;
                    } 
                }     
              
                counter2++;
            }
      
            counter1++;
        }    
    }
    
    // assign exchange rate accordingly (FINALLY!!!)
    if (haveDisciplineToExchange == TRUE) {
        exchangeRate = 2;
    } else if (haveGeneralExchange == TRUE) {
        exchangeRate = 3;
    } else {
        exchangeRate = 4;
    }    

    return exchangeRate;
}

//[Done]
Node createList(void) {
    Node new = malloc(sizeof (node));
    assert(new != NULL);

    new->next = NULL;
    return new;
}

//[Done]
void destroyList(Node n) {
    //We need to create a temporary Node
    //in order to store the pointer to the
    //next node, a pointer  which we will 
    //then free()
    Node temp;
    Node cur = n;

    while (cur->next != NULL) {
        temp = cur->next;
        free(cur);
        cur = temp;
    }

    free(cur);
    return;
}

//[Done]
void appendList(Node n, action a) { 
    //Make new node
    Node new = malloc(sizeof(node));
    assert(new != NULL);
    new->data = a;
    new->next = NULL;

    //Iterate till the end of the list.
    //Then make the pointer at the end
    //point to our new node.
    Node cur = n;
    while (cur->next != NULL) {
        cur = cur->next;
    }
    cur->next = new;

    return;
}


//A converter function which takes a path and returns
//a coord. This function basically works via cases, and
//is not implemented using too much maths. As such it is
//simple to step through.
//
//Remember, face is relative to the top of the board.
//Hence if you move directly left after facing down, 
//you're now facing right.
//
//      0 
//      ^
//      |
// 3<---+--->1
//      |
//      V
//      2
//
//
coord getCoord(path p) {
    assert(stringEquality(p, "NO_PATH") == FALSE);
    //Coord to hold our current position.
    coord c;
    c.x = 2;
    c.y = 0;
    //the face variable stores the direction
    //we are facing.
    //We start facing downward.
    char face = 2;

    //We can't go backward from the start.
    assert(p[0] != 'B');

    int i = 0;
    while (p[i] != 0) {
        //asserts good,
        //Buffer overflows baaaaaaaad
        assert(i < PATH_LIMIT);

        //Face must be valid.
        assert(face <= 3);
        assert(face >= 0);

        //If we're supposed to move backward:
        if (p[i] == 'B') {
            //Go backward.
            if (face == UP) {
                c.y++;
                face = DOWN;
            } else if (face == RIGHT) {
                c.x--;
                face = LEFT;
            } else if (face == DOWN) {
                c.y--;
                face = UP;
            } else if (face == LEFT) {
                c.x++;
                face = RIGHT;
            }
            //face becomes the opposite of what it was.
            //face = (face + 2) % 4;

        } else if (p[i] == 'L') {
            if (face == UP) {
                //Facing up. is there no row
                //to the left? (since if col mod 2 is 
                //equal to row mod 2, the row is to the
                //right.
                if (c.x % 2 == c.y % 2) {
                    c.y--;
                    face = UP;
                } else {
                    c.x--;
                    face = LEFT;
                }
            } else if (face == RIGHT) {
                //There are never 2 
                //right rows in a row (ha)
                //Hence going left means 
                //moving UP.
                c.y--;
                face = UP;
            } else if (face == DOWN) {
                //Facing down. is there no row
                //to the left (RELATIVE TO NORTH)? 
                //(since if col mod 2 is 
                //equal to row mod 2, the row is to the
                //right.
                if (c.x % 2 == c.y % 2) {
                    c.x++;
                    face = RIGHT;
                } else {
                    c.y++;
                    face = DOWN;
                }
            } else if (face == LEFT) {
                //There are never 2 
                //right rows in a row (ha)
                //Hence going left means 
                //moving UP.
                c.y++;
                face = DOWN;
            }

        } else if (p[i] == 'R') {
            if (face == UP) {
                //Facing up. is there no row
                //to the left? (since if col mod 2 is 
                //equal to row mod 2, the row is to the
                //right.
                if (c.x % 2 == c.y % 2) {
                    c.x++;
                    face = RIGHT;
                } else {
                    c.y--;
                    face = UP;
                }
            } else if (face == RIGHT) {
                //There are never 2 
                //right rows in a row (ha)
                //Hence going left means 
                //moving UP.
                c.y++;
                face = DOWN;
            } else if (face == DOWN) {
                //Facing down. is there no row
                //to the left? (since if col mod 2 is 
                //equal to row mod 2, the row is to the
                //right.
                if (c.x % 2 == c.y % 2) {
                    c.y++;
                    face = DOWN;
                } else {
                    c.x--;
                    face = LEFT;
                }
            } else if (face == LEFT) {
                //There are never 2 
                //right rows in a row (ha)
                //Hence going left means 
                //moving UP.
                c.y--;
                face = UP;
            }
        }
        //printf("Went %d\n", face);
        i++;
    }
    //We have now moved through the board and arrived
    //at the end of the path. Our coordinates are correct!
    return c;
}

void removeItem(int index, path *arr, int length) {
    int i = 0;
    while (i < length) {
        if (i == (length-1)) {
            //printf("hmm\n");
            //At end of array, make last element empty.
            stringCopy(arr[i], "NO_PATH", PATH_LIMIT);

            //printf("strcpy\n");
        } else if (i >= index) {
            //Copy into current element the element after
            //it.
            stringCopy(arr[i], arr[i+1], PATH_LIMIT);
        }
        i++;
    }
    //printf("returned\n");
    return;
}

void addItem(path data, path *arr, int length) {
    int i = 0;
    //I'm not 100% sure, but this code might be able
    //to cause a segfault. i == length, then arr[i]
    //is overflowing. Either way, we're going to fail
    //at the next if statement.
    while (i < length && stringEquality(arr[i],"NO_PATH") == FALSE) {
        i++;
    }
    if (i == length) {
        //There's no space in the array...
        assert(FALSE);
    }
    //printf("Copying \"%s\" into arr[%d]\n", data, i);
    stringCopy(arr[i], data, PATH_LIMIT);
    return;
}




