//runGame.c
//Written by Team Keen 7/5/2014
//This file lets us run through our game step by step
//and asks each player what action to take

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>
#include "Game.h"

#define NONE "\033[m"
#define RED "\033[0;32;31m"
#define LIGHT_RED "\033[1;31m"
#define LIGHT_GREEN "\033[1;32m"
#define BLUE "\033[0;32;34m"
#define LIGHT_BLUE "\033[1;34m"
#define LIGHT_CYAN "\033[1;36m"
#define PURPLE "\033[0;35m"
#define LIGHT_PURPLE "\033[1;35m"
#define YELLOW "\033[1;33m"

#define DISC {STUDENT_BQN,STUDENT_MMONEY,STUDENT_MJ,STUDENT_MMONEY,STUDENT_MJ,STUDENT_BPS,STUDENT_MTV,STUDENT_MTV,STUDENT_BPS,STUDENT_MTV,STUDENT_BQN,STUDENT_MJ,STUDENT_BQN,STUDENT_THD,STUDENT_MJ,STUDENT_MMONEY,STUDENT_MTV,STUDENT_BQN,STUDENT_BPS}
#define DICE {9,10,8,12,6,5,3,11,3,11,4,6,4,7,9,2,8,10,5} 
#define MAX_KPI 150

int rollDice(void);
action getMove(Game g);
void cls(void);

//helping function for print map (Shamelessly copied from
//the badass who is Lyans Tao :)
static void printMap(Game g); 
static void printARC (int edge, int edgeID);
static void printregion (Game g, int regionID);
static void printCampus (int vertex);

int main (int argc, char *argv[]) {
    srand(time(NULL));

    int disc[] = DISC;
    int dice[] = DICE;
    Game g = newGame(disc, dice);
    action a;
    int diceVal;

    
    while(getKPIpoints(g, getWhoseTurn(g)) < MAX_KPI) {
        //Set value to not be PASS (so we loop)
        a.actionCode = -1;
        //Roll dice and tell the game the result.
        diceVal = rollDice() + rollDice();
        throwDice(g, diceVal);
        cls();
        printf("A %d is rolled.\n",diceVal);
        printf("It's now turn %d, player %d's turn.\n",getTurnNumber(g) ,getWhoseTurn(g));
        //Let player make a decision until they pass.

        while (a.actionCode != PASS) {
            
            printMap(g);
            //get the action.
            a = getMove(g);

            //If spinoff, decide whether it's IP
            //or a paper.
            if(a.actionCode == START_SPINOFF) {
                //Remembering rollDice returns a number from
                //1 to 6, there is 1/3 chance to roll a number
                //less than or equal to 2.
                if (rollDice() > 3) {
                    a.actionCode = OBTAIN_PUBLICATION;
                } else {
                    a.actionCode = OBTAIN_IP_PATENT;
                }
            }
            makeAction(g, a);
        }
    }
    printf("Player %d wins with %d KPI points!\n",getWhoseTurn(g),
            getKPIpoints(g, getWhoseTurn(g)));
    disposeGame(g);
    return EXIT_SUCCESS;
}

void cls(void) {
    int i = 0;
    while (i < 80) {
        printf("\n");
        i++;
    }
    return;
}

int rollDice(void) {
    return (rand() % 6) + 1;
}

action getMove(Game g) {
    action a;
    printf("You have resources:\n"
            "BPS %d BQN %d\n"
            "MJ  %d MTV %d\n"
            "M$$ %d THD %d\n",
            getStudents(g,getWhoseTurn(g),1),
            getStudents(g,getWhoseTurn(g),2),
            getStudents(g,getWhoseTurn(g),3),
            getStudents(g,getWhoseTurn(g),4),
            getStudents(g,getWhoseTurn(g),5),
            getStudents(g,getWhoseTurn(g),0));
    printf("The action codes are:\n"
            "PASS          0   BUILD CAMPUS    1\n"
            "BUILD GO8     2   OBTAIN ARC      3\n"
            "START SPINOFF 4   RETRAIN STUENTS 7\n");
    printf("Enter an action: ");
    //scanf("%d",&a.actionCode);
    a.actionCode = getchar();
    
    while (a.actionCode == '\n') {
        a.actionCode = getchar();
    }
    a.actionCode -= '0';

    

    if (a.actionCode < 4 && a.actionCode > 0) {
        printf("Please enter a path: "); 
        scanf("%s",a.destination);
    }
    while (isLegalAction(g, a) == FALSE) { 
        printf("Illegal action. Please enter an action: ");
        scanf("%d",&a.actionCode);
        if (a.actionCode < 3 && a.actionCode > 0) {
            printf("Please enter a path: "); 
            scanf("%s",a.destination);
        }
    }
    return a;
}

//print map
static void printMap(Game g) {
    printf("                   "LIGHT_GREEN"RC"NONE"     ");
    printCampus(getCampus(g,""));
    printARC(getARC(g,"L"),1);
    printf("%d     "LIGHT_PURPLE"RC"NONE"\n",getCampus(g,"L"));
    printf("                  /  \\   ");
    printARC(getARC(g,"R"),2);
    printf("        ");
    printARC(getARC(g,"LR"),3);
    printf("   /  \\\n");
    printf("                 /    \\ ");
    printARC(getARC(g,"R"),2);
    printf("          ");
    printARC(getARC(g,"LR"),3);
    printf(" /    \\\n");
    printf("                ");
    printCampus(getCampus(g,"RR"));
    printARC(getARC(g,"RR"),1);
    printCampus(getCampus(g,"R"));
    printf("     ");
    printregion(g,7);
    printf("     ");
    printCampus(getCampus(g, "LR"));
    printARC(getARC(g,"LRL"),1);
    printCampus(getCampus(g,"LRL"));
    printf("\n               ");
    printARC(getARC(g,"RRL"),2);
    printf("        ");
    printARC(getARC(g,"RL"),3);
    printf("          ");
    printARC(getARC(g,"LRR"),2);
    printf("        ");
    printARC(getARC(g,"RLRL"),3);
    printf("\n              ");
    printARC(getARC(g,"RRL"),2);
    printf("          ");
    printARC(getARC(g,"RL"),3);
    printf("        ");
    printARC(getARC(g,"LRR"),2);
    printf("          ");
    printARC(getARC(g,"RLRL"),3);
    printf("\n");
    printf("      ");
    printCampus(getCampus(g,"RRLR"));
    printARC(getARC(g,"RRLR"),1);
    printCampus(getCampus(g,"RRL"));
    printf("     ");
    printregion(g,3);
    printf("     ");
    printCampus(getCampus(g,"RL"));
    printARC(getARC(g,"RLL"),1);
    printCampus(getCampus(g,"RLL"));
    printf("     ");
    printregion(g,12);
    printf("     ");
    printCampus(getCampus(g,"LRLR"));
    printARC(getARC(g,"LRLRL"),1);
    printCampus(getCampus(g,"LRLRL"));
    printf("\n     ");
    printARC(getARC(g,"RRLRL"),2);
    printf("        ");
    printARC(getARC(g,"RRLL"),3);
    printf("          ");
    printARC(getARC(g,"RLR"),2);
    printf("        ");
    printARC(getARC(g,"LRRL"),3);
    printf("          ");
    printARC(getARC(g,"LRLRR"),2);
    printf("        ");
    printARC(getARC(g,"LRLRLR"),3);
    printf("\n    ");
    printARC(getARC(g,"RRLRL"),2);
    printf("          ");
    printARC(getARC(g,"RRLL"),3);
    printf("        ");
    printARC(getARC(g,"RLR"),2);
    printf("          ");
    printARC(getARC(g,"LRRL"),3);
    printf("        ");
    printARC(getARC(g,"LRLRR"),2);
    printf("          ");
    printARC(getARC(g,"LRLRLR"),3);   
    printf("\n   ");
    printCampus(getCampus(g,"RRLRL"));
    printf("     ");
    printregion(g,0);
    printf("     ");
    printCampus(getCampus(g,"RRLL"));
    printARC(getARC(g,"RLRR"),1);
    printCampus(getCampus(g,"RLR"));
    printf("     ");
    printregion(g,8);
    printf("     ");
    printCampus(getCampus(g,"RLLR"));
    printARC(getARC(g,"RLLRL"),1);
    printCampus(getCampus(g,"LRLRR"));
    printf("     ");
    printregion(g,16);
    printf("     ");
    printCampus(getCampus(g,"LRLRLR"));
    printf("\n    ");
    printARC(getARC(g,"RRLRLL"),3);
    printf("          ");
    printARC(getARC(g,"RLRRL"),2);
    printf("        ");
    printARC(getARC(g,"RLRL"),3);
    printf("          ");
    printARC(getARC(g,"RLLRR"),2);
    printf("        ");
    printARC(getARC(g,"LRLRRL"),3);
    printf("          ");
    printARC(getARC(g,"LRLRLRR"),2);
    printf("\n     ");
    printARC(getARC(g,"RRLRLL"),3);
    printf("        ");
    printARC(getARC(g,"RLRRL"),2);
    printf("          ");
    printARC(getARC(g,"RLRL"),3);
    printf("        ");
    printARC(getARC(g,"RLLRR"),2);
    printf("          ");
    printARC(getARC(g,"LRLRRL"),3);
    printf("        ");
    printARC(getARC(g,"LRLRLRR"),2);

    printf("\n");
    printf("      ");
    printCampus(getCampus(g,"RLRRLR"));
    printARC(getARC(g,"RLRRLR"),1);
    printCampus(getCampus(g,"RLRRL"));
    printf("     ");
    printregion(g,4);
    printf("     ");
    printCampus(getCampus(g,"RLRL"));
    printARC(getARC(g,"RLRLL"),1);
    printCampus(getCampus(g,"RLRLL"));
    printf("     ");
    printregion(g,13);
    printf("     ");
    printCampus(getCampus(g,"RLLRLR"));
    printARC(getARC(g,"RLLRLRL"),1);
    printCampus(getCampus(g,"RLLRLRL"));
    printf("\n     ");
    printARC(getARC(g,"RLRRLRL"),2);
    printf("        ");
    printARC(getARC(g,"RLRRLL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLR"),2);
    printf("        ");
    printARC(getARC(g,"RLLRRL"),3);
    printf("          ");
    printARC(getARC(g,"RLLRLRR"),2);
    printf("        ");
    printARC(getARC(g,"RLLRLRLR"),3);
    printf("\n    ");
    printARC(getARC(g,"RLRRLRL"),2);
    printf("          ");
    printARC(getARC(g,"RLRRLL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLR"),2);
    printf("          ");
    printARC(getARC(g,"RLLRRL"),3);
    printf("        ");
    printARC(getARC(g,"RLLRLRR"),2);
    printf("          ");
    printARC(getARC(g,"RLLRLRLR"),3);
    printf("\n   ");
    printCampus(getCampus(g,"RLRRLRL"));
    printf("     ");
    printregion(g,1);
    printf("     ");
    printCampus(getCampus(g,"RLRRLL"));
    printARC(getARC(g,"RLRLRR"),1);
    printCampus(getCampus(g,"RLRLR"));
    printf("     ");
    printregion(g,9);
    printf("     ");
    printCampus(getCampus(g,"RLRLLR"));
    printARC(getARC(g,"RLRLLRL"),1);
    printCampus(getCampus(g,"RLLRLRR"));
    printf("     ");
    printregion(g,17);
    printf("     ");
    printCampus(getCampus(g,"RLLRLRLR"));
    printf("\n    ");
    printARC(getARC(g,"RLRRLRLL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLRRL"),2);
    printf("        ");
    printARC(getARC(g,"RLRLRL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLLRR"),2);
    printf("        ");
    printARC(getARC(g,"RLLRLRRL"),3);
    printf("          ");
    printARC(getARC(g,"RLLRLRLRR"),2);
    printf(" \\\n     ");
    printARC(getARC(g,"RLRRLRLL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLRRL"),2);
    printf("          ");
    printARC(getARC(g,"RLRLRL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLLRR"),2);
    printf("          ");
    printARC(getARC(g,"RLLRLRRL"),3);
    printf("        ");
    printARC(getARC(g,"RLLRLRLRR"),2); 

    printf("   \\\n");
    printf("      ");
    printCampus(getCampus(g,"RLRLRRLR"));
    printARC(getARC(g,"RLRLRRLR"),1);
    printCampus(getCampus(g,"RLRLRRL"));
    printf("     ");
    printregion(g,5);
    printf("     ");
    printCampus(getCampus(g,"RLRLRL"));
    printARC(getARC(g,"RLRLRLL"),1);
    printCampus(getCampus(g,"RLRLRLL"));
    printf("     ");
    printregion(g,14);
    printf("     ");
    printCampus(getCampus(g,"RLRLLRLR"));
    printARC(getARC(g,"RLRLLRLRL"),1);
    printCampus(getCampus(g,"RLRLLRLRL"));
    printf("____"LIGHT_CYAN"RC"NONE"\n     ");
    printARC(getARC(g,"RLRLRRLRL"),2);
    printf("        ");
    printARC(getARC(g,"RLRLRRLL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLRLR"),2);
    printf("        ");
    printARC(getARC(g,"RLRLLRRL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLLRLRR"),2);
    printf("        ");
    printARC(getARC(g,"RLRLLRLRLR"),3);
    printf("\n    ");
    printARC(getARC(g,"RLRLRRLRL"),2);
    printf("          ");
    printARC(getARC(g,"RLRLRRLL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLRLR"),2);
    printf("          ");
    printARC(getARC(g,"RLRLLRRL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLLRLRR"),2);
    printf("          ");
    printARC(getARC(g,"RLRLLRLRLR"),3);
    printf("\n   ");
    printCampus(getCampus(g,"RLRLRRLRL"));
    printf("     ");
    printregion(g,2);
    printf("     ");
    printCampus(getCampus(g,"RLRLRRLL"));
    printARC(getARC(g,"RLRLRLRR"),1);
    printCampus(getCampus(g,"RLRLRLR"));
    printf("     ");
    printregion(g,10);
    printf("     ");
    printCampus(getCampus(g,"RLRLRLLR"));
    printARC(getARC(g,"RLRLRLLRL"),1);
    printCampus(getCampus(g,"RLRLLRLRR"));
    printf("     ");
    printregion(g,18);
    printf("     ");
    printCampus(getCampus(g,"RLRLLRLRLR"));
    printf("\n    ");
    printARC(getARC(g,"RLRLRRLRLL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLRLRRL"),2);
    printf("        ");
    printARC(getARC(g,"RLRLRLRL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLRLLRR"),2);
    printf("        ");
    printARC(getARC(g,"RLRLLRLRRL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLLRLRLRR"),2);
    printf("\n     ");
    printARC(getARC(g,"RLRLRRLRLL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLRLRRL"),2);
    printf("          ");
    printARC(getARC(g,"RLRLRLRL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLRLLRR"),2);
    printf("          ");
    printARC(getARC(g,"RLRLLRLRRL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLLRLRLRR"),2);

    printf("\n");
    printf("      ");
    printCampus(getCampus(g,"RLRLRLRRLR"));
    printARC(getARC(g,"RLRLRLRRLR"),1);
    printCampus(getCampus(g,"RLRLRLRRL"));
    printf("     ");
    printregion(g,6);
    printf("     ");
    printCampus(getCampus(g,"RLRLRLRL"));
    printARC(getARC(g,"RLRLRLRLL"),1);
    printCampus(getCampus(g,"RLRLRLRLL"));
    printf("     ");
    printregion(g,15);
    printf("     ");
    printCampus(getCampus(g,"RLRLRLLRLR"));
    printARC(getARC(g,"RLRLRLLRLRL"),1);
    printCampus(getCampus(g,"RLRLRLLRLRL"));

    printf("\n            / ");
    printARC(getARC(g,"RLRLRLRRLL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLRLRLR"),2);
    printf("        ");
    printARC(getARC(g,"RLRLRLRLLR"),3);
    printf("          ");
    printARC(getARC(g,"RLRLRLLRLRR"),2);
    printf(" \\\n           /   ");
    printARC(getARC(g,"RLRLRLRRLL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLRLRLR"),2);
    printf("          ");
    printARC(getARC(g,"RLRLRLRLLR"),3);
    printf("        ");
    printARC(getARC(g,"RLRLRLLRLRR"),2);

    printf("   \\\n");
    printf("          "LIGHT_RED"RC"NONE"____");
    printCampus(getCampus(g,"RLRLRLRLRR"));
    printARC(getARC(g,"RLRLRLRLRR"),1);
    printCampus(getCampus(g,"RLRLRLRLR"));
    printf("     ");
    printregion(g,11);
    printf("     ");
    printCampus(getCampus(g, "RLRLRLRLLR"));
    printARC(getARC(g,"RLRLRLRLLRL"),1);
    printCampus(getCampus(g,"RLRLRLRLLRL"));
    printf("____"YELLOW"RC"NONE"\n");

    printf("                        ");
    printARC(getARC(g,"RLRLRLRLRL"),3);
    printf("          ");
    printARC(getARC(g,"RLRLRLRLLRR"),2);
    printf("\n                         ");
    printARC(getARC(g,"RLRLRLRLRL"),3);
    printf("        ");
    printARC(getARC(g,"RLRLRLRLLRR"),2);
    printf("\n                          ");
    printCampus(getCampus(g,"RLRLRLRLRL"));
    printARC(getARC(g,"RLRLRLRLRLL"),1);
    printCampus(getCampus(g,"RLRLRLRLRLL"));
    printf("\n");

    printf(NONE);
}

//helping function for print map
static void printARC (int edge, int edgeID) {
    if (edgeID == 1) {
        if(edge == ARC_A) {
            printf(RED   "______");
        } else if (edge == ARC_B) {
            printf(BLUE  "______");
        } else if (edge == ARC_C) {
            printf(PURPLE"______");
        } else {
            printf(NONE  "______");
        }
    } else if (edgeID == 2) {
        if(edge == ARC_A) {
            printf(RED   "/");
        } else if (edge == ARC_B) {
            printf(BLUE  "/");
        } else if (edge == ARC_C) {
            printf(PURPLE"/");
        } else {
            printf(NONE  "/");
        }
    } else if (edgeID == 3) {
        if(edge == ARC_A) {
            printf(RED   "\\");
        } else if (edge == ARC_B) {
            printf(BLUE  "\\");
        } else if (edge == ARC_C) {
            printf(PURPLE"\\");
        } else {
            printf(NONE  "\\");
        }
    }
    printf(NONE);
}

static void printregion (Game g, int regionID) {
    int regionDP = getDiscipline(g, regionID);
    int regionDV = getDiceValue(g, regionID);
    if (regionDP == 0) {
        printf(LIGHT_BLUE"%d",regionDV);
    } else if (regionDP == 1) {
        printf(LIGHT_RED"%d",regionDV);
    } else if (regionDP == 2) {
        printf(LIGHT_CYAN"%d",regionDV);
    } else if (regionDP == 3) {
        printf(YELLOW"%d",regionDV);
    } else if (regionDP == 4) {
        printf(LIGHT_GREEN"%d",regionDV);
    } else {
        printf(LIGHT_PURPLE"%d",regionDV);
    }
    if (regionDV < 10) {
        printf(NONE" ");
    }
    printf(NONE);
}

static void printCampus (int vertex) {
    if (vertex == 1) {
        printf(RED"C");
    } else if (vertex == 2) {
        printf(BLUE"C");
    } else if (vertex == 3) {
        printf(PURPLE"C");
    } else if (vertex == 4) {
        printf(RED"G");
    } else if (vertex == 5) {
        printf(BLUE"G");
    } else if (vertex == 6) {
        printf(PURPLE"G");
    } else {
        printf(NONE"0");
    }
    printf(NONE);
}
