//testGame.c
//Written by team keen! 3/5/2014
//This program is used to test the game type defined in Game.c and Game.h
//To run it:
//gcc -Wall -Werror -O testGame.c Game.c && ./a.out

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "Game.h"

#define MAX_TURNS 9000

//This block is testEmptyGame and its sub functions
void testEmptyGame (void);
void testStartingState (game g);
void testFirstTurn (game g, action a);
void testInsufficientStudents (game g, action a);

void testTurnCounting (void);

int main(int argc, char* argv[]) {
    
    testEmptyGame();
    testTurnCounting();
    printf("All tests passed. You are awesome!\n");
    return EXIT_SUCCESS;
}

//This creates a game that generates no students
//It will test creation with no resources
//It also tests that the starting state of a new game is correct
void testEmptyGame (void) {
    //Only get thds so need to manually add students for testing
    int discipline[NUM_REGIONS] = {0};
    int dice[NUM_REGIONS] = {0};

    //test a new game is created properly
    Game g = newGame(discipline, dice);
    assert(g != NULL);

    //Big test block
    //See function names and the functions for what they test
    testStartingState(g);
    action a;
    testFirstTurn(g,a);
    testInsufficientStudents (g, a);

    //test a game is properly removed
    disposeGame(g);
    assert(g == NULL);
    printf("Can create and dispose Game\n isLegalAction does not allow"
        " creation with insufficient resources\n\n");
}

void testStartingState (game g) {
    //test turn number is correct, i.e we are in terra nullius mode
    assert(getTurnNumber(g) == -1);
    assert(getWhoseTurn(g) == NO_ONE);
    //test the campus at null path (eg A1) is player a's campus
    path p = {0};
    assert(getCampus(g, p) == 1);
    //nobody should be winning in terms of arcs or papers
    assert(getMostARCs(g) == NO_ONE);
    assert(getMostPublications(g) == NO_ONE);
    //everybody should have 24 points (2 campuses and 2 arcs)
    assert(getKPIpoints (g, 1) == 24);
    assert(getKPIpoints (g, 2) == 24);
    assert(getKPIpoints (g, 3) == 24);
    //everybody has 2 arcs 2 campuses 
    //and no Go8's, IPs, papers, students
    assert(getCampuses (g, 1) == 2);
    assert(getARCs (g, 1) == 0);
    assert(getGO8s (g, 1) == 0);
    assert(getIPs (g, 1) == 0);
    assert(getPublications(g, 1) == 0);
    assert(getStudents (g, 1, 1) == 0);
    assert(getCampuses (g, 2) == 2);
    assert(getARCs (g, 2) == 0);
    assert(getGO8s (g, 2) == 0);
    assert(getIPs (g, 2) == 0);
    assert(getPublications(g, 2) == 0);
    assert(getStudents (g, 2, 1) == 0);
    assert(getCampuses (g, 3) == 2);
    assert(getARCs (g, 3) == 0);
    assert(getGO8s (g, 3) == 0);
    assert(getIPs (g, 3) == 0);
    assert(getPublications(g, 3) == 0);
    assert(getStudents (g, 3, 1) == 0);
}

void testFirstTurn (game g, action a) {
    //test actions
    //test that it's player 1's turn
    throwDice(g, 7);
    assert(getTurnNumber(g) == 0);
    assert(getWhoseTurn(g) == 1);
    a.actionCode = PASS;
    //we should be able to pass...
    assert(isLegalAction(g, a) == TRUE);
    makeAction(g, a);
}

void testInsufficientStudents (game g, action a) {
    //Next person's turn!
    throwDice(g, 7);
    assert(getTurnNumber(g) == 1);
    assert(getWhoseTurn(g) == 2);
    //we should NOT be allowed to make a campus...
    a.actionCode = BUILD_CAMPUS;
    a.destination[0] = 'L';
    a.destination[1] = 'R';
    a.destination[2] = '\0';
    assert(isLegalAction(g, a) == FALSE);
    //We should not be able to create a GO8
    a.actionCode = BUILD_GO8;
    assert(isLegalAction(g, a) == FALSE);
    //We should not be able to build an ARC
    a.actionCode = OBTAIN_ARC;
    assert(isLegalAction(g, a) == FALSE);
    //Nor start spinoffs
    a.actionCode = START_SPINOFF;
    assert(isLegalAction(g, a) == FALSE);
    //Nor get their results
    a.actionCode = OBTAIN_PUBLICATION;
    assert(isLegalAction(g, a) == FALSE);
    a.actionCode = OBTAIN_IP_PATENT;
    assert(isLegalAction(g, a) == FALSE);
    //Nor retrain students of any to any
    a.actionCode = RETRAIN_STUDENTS;
    int from = STUDENT_THD;
    int to = STUDENT_THD;
    while (from <= STUDENT_MMONEY) {
        while (to <= STUDENT_MMONEY) {
            a.disciplineTo = to;
            a.disciplineFrom = from;
            assert(isLegalAction(g, a) == FALSE);
            to++;
        }
        to = STUDENT_THD;
        from++;
    }
}

void testTurnCounting (void) {
    int turn = 0;
    int whoseTurn = 0;
    int counter = 0;
    int discipline[NUM_REGIONS] = {0};
    int dice[NUM_REGIONS] = {0};
    Game g = newGame(discipline, dice);
    assert(g != NULL);
    while (counter < MAX_TURNS) {
        throwDice(g,7);
        //Using whoseturn +1 for ease in incrementing
        //This means that I can use mod in counting whose turn
        //getWhoseTurn should return a value 1-3
        assert(getWhoseTurn(g)==(whoseTurn+1));
        assert(getTurnNumber(g)==turn);
        turn++;
        whoseTurn++;
        whoseTurn = whoseTurn%3;
        counter++;
    }
    printf("Turn counting works\n");
    disposeGame(g);
}