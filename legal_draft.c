//isLegal draft
int canBuild (Game g, action a, int G08);
int neighbouringARCs (Game g, action a);

int isLegalAction (Game g, action a){
    int player = getWhoseTurn(g);
    int flag = FALSE;
    int counter = 0;
    int actionCode = a.actionCode; //for typing ease silly in hindsight

    //below checks you have the prerequisits to build/obtain/retrain
    //each if checks an action option
    //this series of if statements does NOT check that the location is on the board
    //to do an action you must have the required students
    //and the required prebuilt structures (ie you need an ARC for a campus)
    if (actionCode == PASS) {
        flag = TRUE;

    } else if (actionCode == BUILD_CAMPUS) {
        flag = canBuild(g,a,FALSE);
        //below checks for insufficient of required students
        //since only one of each type is required if 0 is the only fail
        //canBuild has already set to true if can build
        if(g->players[player].students[STUDENT_BPS]==0
            ||g->players[player].students[STUDENT_BQN]==0
            ||g->players[player].students[STUDENT_MJ]==0
            ||g->players[player].students[STUDENT_MTV]==0){
                flag = FALSE;
        }

    } else if (actionCode == BUILD_GO8) {
        flag = canBuild(g,a,TRUE);
        //below checks for sufficient students
        //canBuild has already set to true if can build
        if (g->players[player].students[STUDENT_MJ]<2
            ||g->players[player].students[STUDENT_MMONEY]<3) {
            flag = FALSE;
        }

    } else if (actionCode == OBTAIN_ARC) {
        //checking there isnt an ARC there already
        flag=TRUE;
        while (counter<ARC_LIMIT) {
            if (stringEquality(g->players[player].arcs[counter], a.destination)) {
                flag = FALSE;
            }
            counter++;
        }
        //checking for insufficient students
        if (g->players[player].students[STUDENT_BQN]==0
            ||g->players[player].students[STUDENT_BPS]==0){
            flag = FALSE;
        }
        //NEED TO CHECK FOR NEIGHBOURING ARCS
        if (!neighbouringARCs(g,a)) {
            //no neighbouring arcs
            flag = FALSE;
        }
        
        assert(FALSE);
    } else if (actionCode == START_SPINOFF) {
        //Game.c cant start spinoffs the ai does that
        //run game decides if it is a publication or patent
        flag=FALSE;
        
    } else if (actionCode == OBTAIN_PUBLICATION) {
        if (g->players[player].students[STUDENT_MJ]==0
            ||g->players[player].students[STUDENT_MTV]==0
            ||g->players[player].students[STUDENT_MMONEY]==0){
            flag = FALSE;
        } else {
            flag = TRUE;
        }

    } else if (actionCode == OBTAIN_IP_PATENT) {
        if (g->players[player].students[STUDENT_MJ]==0
            ||g->players[player].students[STUDENT_MTV]==0
            ||g->players[player].students[STUDENT_MMONEY]==0){
            flag = FALSE;
        } else {
            flag = TRUE;
        }

    } else if (actionCode == RETRAIN_STUDENTS) {
        //checking for sufficient students
        int exchange = getExchangeRate (g, player, a.disciplineFrom, a.disciplineTo)
        if (g->players[player].students[a.disciplineFrom] >= exchange) {
            flag = TRUE;
        } 

    }
    //checks location is valid
    //only sets to false as if it is possible it will be set to true above
    if (actionCode == BUILD_CAMPUS||actionCode == OBTAIN_ARC
            ||actionCode == BUILD_GO8) {
        coord position = getCoord(a.destination);
        //check coordinate is on the map
        
        if (position.x==0||position.x==5) {
            if (position.y==0||position.y==1||position.y==9||position.y==10){
                flag = FALSE;
            }
        } else if (position.x == 1||position.x==4) {
            if (position.y==0||position.y==10){
                flag = FALSE;
            }
        }
    }

    
    return flag;
}

//this checks building resources
//the G08 variable allows this function to work for normal and G08 campuses
int canBuild (Game g, action a, int G08) {
    int counter = 0;
    int flag = FALSE;

    if (getARC(g, a.destination) == player) {
        flag = TRUE;
    }

    while (counter<CAMPUS_LIMIT) {
        if (stringEquality(g->players[player].campus[counter], a.destination)) {
            if (G08) {
                flag = TRUE;
            } else {
                flag = FALSE;
            }
        }
            counter++;
    }
    counter=0;
    while (counter<GO8_LIMIT) {
        if (stringEquality(g->players[player].go8s[counter], a.destination)) {
            flag = FALSE;
        }
        counter++;
    }
    return flag;
}

//This function takes in a path and modifies it to refer to the adjacent edges
//It then uses getARC to see who (if anyone) owns arcs on those edges
//If the current player owns any arcs on the edges this will return true
int neighbouringARCs (Game g, action a) {
    int player = getWhoseTurn(g);
    path pathToMod;
    int flag = FALSE;
    int counter = 0;
    //making a copy of the destination string
    while (counter < PATH_LIMIT) {
        pathToMod[counter] = a.destination[counter];
    }
    //these will store paths for the 4 abjacent edges
    path option1;
    path option2;
    path option3;
    path option4;

    int changed = FALSE;
    counter = 0; //resetting for another parse
    //making first option
    while (changed = FALSE) {
        option1[counter] = pathToMod[counter];
        if (option1[counter] != 'L' && option1[counter] != 'R' && option1[counter] != 'B') {
            option1[counter] = 'L';
            changed = TRUE;
        }
        counter++;
    }

    counter = 0;
    changed = FALSE;
    while (changed = FALSE) {
        option2[counter] = pathToMod[counter];
        if (option2[counter] != 'L' && option2[counter] != 'R' && option2[counter] != 'B') {
            option2[counter] = 'R';
            changed = TRUE;
        }
        counter++;
    }

    counter = 0;
    changed = FALSE;
    while (changed = FALSE) {
        option3[counter] = pathToMod[counter];
        if (option3[counter] != 'L' && option3[counter] != 'R' && option3[counter] != 'B') {
            option3[counter] = 'B';
            option3[counter] = 'L';
            changed = TRUE;
        }
        counter++;
    }

    counter = 0;
    changed = FALSE;
    while (changed = FALSE) {
        option4[counter] = pathToMod[counter];
        if (option4[counter] != 'L' && option4[counter] != 'R' && option4[counter] != 'B') {
            option4[counter] = 'B';
            option4[counter] = 'R';
            changed = TRUE;
        }
        counter++;
    }

    if (getARC(g, option1) == player) {
        flag = TRUE;
    } else if (getARC(g, option2) == player) {
        flag = TRUE;
    } else if (getARC(g, option3) == player) {
        flag = TRUE;
    } else if (getARC(g, option4) == player) {
        flag = TRUE;
    }
    return flag;
}